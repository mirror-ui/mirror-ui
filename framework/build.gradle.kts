import org.jetbrains.compose.compose
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    id("org.jetbrains.compose")
}

group = "dev.superboring.mirror"
version = "1.0.0"

repositories {
    mavenCentral()
}

val ktorVersion = rootProject.ext["ktorVersion"]

dependencies {
    implementation(kotlin("stdlib"))
    implementation(when (System.getenv("ARCH")) {
        "arm64" -> {
            compose.desktop.linux_arm64
        }
        else -> {
            println("Building for current OS (default)")
            compose.desktop.currentOs
        }
    }.also { println("Selected compose platform $it") })
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.6.10")
    implementation("com.charleskorn.kaml:kaml:0.52.0")
    implementation("io.github.microutils:kotlin-logging:3.0.5")

    compileOnly("io.ktor:ktor-client-core:$ktorVersion")
    compileOnly("io.ktor:ktor-client-java:$ktorVersion")
    compileOnly("io.ktor:ktor-client-auth:$ktorVersion")
    compileOnly("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
    compileOnly("io.ktor:ktor-client-content-negotiation:$ktorVersion")
}

task("frameworkJar", type = Jar::class) {
    archiveBaseName.set("framework")
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    with(tasks["jar"] as CopySpec)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    destinationDirectory.set(File("$rootDir/dist/framework"))
}