package framework.decoration

import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberUpdatedState
import lib.ui.theme.colors

object DefaultWindowDecoration : WindowDecoration {
    @Composable
    override fun navbarBackgroundState() = rememberUpdatedState(colors().background)

    @Composable
    override fun windowBackgroundState() = rememberUpdatedState(colors().background)
}