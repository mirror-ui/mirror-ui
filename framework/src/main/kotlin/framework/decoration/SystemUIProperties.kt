package framework.decoration

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

object SystemUIProperties {

    var navbarHeight by mutableStateOf(48.dp)
        private set

    @Composable
    fun Modifier.navbarHeight() = run { height(navbarHeight) }

    @Composable
    fun Modifier.navbarBottomPadding() = run { padding(bottom = navbarHeight) }

}