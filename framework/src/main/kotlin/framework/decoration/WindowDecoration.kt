package framework.decoration

import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.graphics.Color

interface WindowDecoration {
    @Composable
    fun navbarBackgroundState(): State<Color>
    @Composable
    fun windowBackgroundState(): State<Color>
}