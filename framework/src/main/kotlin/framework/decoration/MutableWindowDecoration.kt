package framework.decoration

import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.Color

class MutableWindowDecoration : WindowDecoration {
    @Composable
    override fun navbarBackgroundState() =
        if (mutableNavbarBackground.value == Color.Unspecified) DefaultWindowDecoration.navbarBackgroundState()
        else mutableNavbarBackground

    @Composable
    override fun windowBackgroundState() =
        if (mutableWindowBackground.value == Color.Unspecified) DefaultWindowDecoration.windowBackgroundState()
        else mutableWindowBackground

    @Composable
    fun navbarBackground() = navbarBackgroundState().value
    @Composable
    fun windowBackground() = windowBackgroundState().value

    private var mutableNavbarBackground = mutableStateOf(Color.Unspecified)
    private var mutableWindowBackground = mutableStateOf(Color.Unspecified)

    @Composable
    fun setNavbarBackground(color: Color) = apply {
        mutableNavbarBackground.value = color
    }

    @Composable
    fun setWindowBackground(color: Color) = apply {
        mutableWindowBackground.value = color
    }
}