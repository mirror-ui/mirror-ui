package framework.pkg

import core.app.manifest.Manifest
import core.systemui.SystemUIBase
import core.windowing.WindowManager.prepare
import framework.app.RecentsActivity
import framework.widget.Widget
import lib.reflect.asObject
import logger
import java.nio.file.Files
import java.util.*
import java.util.zip.ZipFile
import kotlin.io.path.Path
import kotlin.io.path.extension
import kotlin.io.path.isRegularFile
import kotlin.streams.toList

object PackageManager {

    private val packageFiles
        get() =
            Files.walk(Path("./apps"))
                .filter { it.isRegularFile() }
                .filter { it.extension == "jar" }
                .map { it.toFile() }
                .toList()

    // Packages load on first access and stay. Packages added during runtime must be added to the list.
    // Reloading is not allowed because it would discard the state of all apps.
    private val packageList by lazy { LinkedList<Package>().apply { addAll(loadPackages()) } }
    val packages get() = packageList.toList()

    private fun loadPackages(): List<Package> {
        return packageFiles.map {
            Package(it.canonicalPath).also { pkg ->
                ZipFile(it).use { zipFile ->
                    zipFile.run {
                        val manifestEntry = getEntry("manifest.yaml")
                        val loadedManifest = getInputStream(manifestEntry).use { manifestStream ->
                            Manifest.load(manifestStream)
                        }
                        pkg.apply {
                            manifest = loadedManifest
                        }
                        logger.info("Found package ${pkg.packageName}")
                    }
                }
            }
        }
    }

    fun byName(packageName: String) = packages.find { it.packageName == packageName }

    val widgets: List<Widget>
        get() = packages.flatMap { pkg ->
            pkg.manifest.provides.widgets.map { w ->
                w.widgetView.asObject<Widget>().apply {
                    app = pkg.app
                }
            }
        }

    val recents: List<RecentsActivity>
        get() = packages.flatMap { pkg ->
            pkg.manifest.provides.recents.map { w ->
                w.recentsActivity.asObject()
            }
        }

    val systemUI: SystemUIBase
        get() = packages
                .find { pkg -> pkg.isSystemApp && pkg.manifest.provides.systemUI != null }
                ?.manifest?.provides?.systemUI?.systemUIClass?.asObject<SystemUIBase>()?.prepare()
            ?: throw IllegalStateException("SystemUI not found")

}