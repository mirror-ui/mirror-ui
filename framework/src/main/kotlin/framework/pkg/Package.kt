package framework.pkg

import core.app.manifest.Manifest
import framework.app.App
import logger

class Package(
    val fileName: String,
) : Packaged {

    val packageName get() = manifest.packageName
    val isSystemApp get() = packageName.startsWith("system.")

    lateinit var manifest: Manifest

    val app by lazy {
        logger.info("Initializing app for package $packageName")
        App(this)
    }
}