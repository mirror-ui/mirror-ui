package framework.widget

import androidx.compose.runtime.Composable
import framework.app.App
import framework.settings.AppSettingsBase

abstract class Widget {
    lateinit var app: App

    @Composable
    abstract fun View()

    inline fun <reified T : AppSettingsBase> settings(): T = app.settings()
}