package framework.settings

import Paths
import lib.annotations.annotation
import lib.extensions.java.nio.file.plus
import lib.serialization.createDefaultYamlIfNotExists
import lib.serialization.deserializeYaml
import java.nio.file.Path
import kotlin.reflect.KClass

object Settings {
    val systemSettings by lazy { loadSettings<SystemSettings>() }

    private inline fun <reified T : SettingsBase> loadSettings(): T =
        T::class.settingsFile().createDefaultYamlIfNotExists<T>().deserializeYaml()

    private fun KClass<out SettingsBase>.settingsFilePath(): Path =
        Paths.settingsDir + "${annotation(SettingsUniqueName::class).name}.yaml"

    private fun KClass<out SettingsBase>.settingsFile() = settingsFilePath().toFile()
}
