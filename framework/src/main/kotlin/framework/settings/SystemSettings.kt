package framework.settings

import framework.pkg.PackageManager
import kotlinx.serialization.Serializable

@SettingsUniqueName("system")
@Serializable
data class SystemSettings(
    val defaultLauncher: DefaultLauncherSetting = DefaultLauncherSetting(),
) : SettingsBase()

@Serializable
data class DefaultLauncherSetting(
    var packageName: String = "",
    var launcherActivity: String = "",
) {
    init {
        if (packageName.isBlank()) {
            // There's no default launcher defined, so we'll just
            // take the first one we find
            val launcherPackage = PackageManager.packages.find { it.manifest.provides.launchers.isNotEmpty() }
                ?: throw NotImplementedError("No launcher found and app chooser has not been implemented yet")
            packageName = launcherPackage.packageName
        }
        if (launcherActivity.isBlank()) {
            val launcherPackage = PackageManager.byName(packageName)
                ?: throw IllegalStateException("Launcher package $packageName not found but it should be there")
            launcherActivity = launcherPackage.manifest.provides.launchers.first().launcherActivity
        }
    }

    fun app() = PackageManager.byName(packageName)!!.app
}
