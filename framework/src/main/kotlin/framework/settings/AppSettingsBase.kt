package framework.settings

import kotlinx.coroutines.runBlocking

abstract class AppSettingsBase : SettingsBase() {
    lateinit var save: suspend () -> Unit
}

suspend inline infix fun <reified A : AppSettingsBase> A.modifications(f: A.() -> Unit) =
    apply { f() }.apply { save() }
inline infix fun <reified A : AppSettingsBase> A.modify(crossinline f: A.() -> Unit) =
    runBlocking { modifications(f) }
