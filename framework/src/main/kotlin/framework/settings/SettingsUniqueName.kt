package framework.settings

@Target(AnnotationTarget.CLASS)
@MustBeDocumented
annotation class SettingsUniqueName(val name: String)
