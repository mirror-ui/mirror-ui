package framework.settings

@Target(AnnotationTarget.CLASS)
@MustBeDocumented
annotation class AppSettingsName(val name: String)
