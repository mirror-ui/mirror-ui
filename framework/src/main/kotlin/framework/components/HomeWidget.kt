package framework.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import lib.ui.theme.colors

@Composable
fun HomeWidget(addPadding: Boolean = true, content: @Composable () -> Unit) {
    Surface(
        shape = RoundedCornerShape(17.dp),
        modifier = Modifier.padding(8.dp),
        color = colors().background.copy(alpha = .25f),
        contentColor = colors().onSurface,
    ) {
        Box(modifier = Modifier.run { if (addPadding) padding(8.dp) else this }) {
            content()
        }
    }
}