package framework.windowing

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.unit.Dp

class WindowCustomization {
    var drawBehindSystemBars by mutableStateOf(false)
    var windowBackgroundBlur by mutableStateOf(null as Dp?)
}