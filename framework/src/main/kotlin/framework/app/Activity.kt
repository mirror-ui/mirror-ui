package framework.app

import androidx.compose.runtime.Composable
import framework.decoration.MutableWindowDecoration
import framework.settings.AppSettingsBase
import framework.windowing.WindowCustomization

abstract class Activity {
    lateinit var app: App
    val windowDecoration = MutableWindowDecoration()
    val windowCustomization = WindowCustomization()

    @Composable
    abstract fun Root()

    @Composable
    operator fun invoke() {
        Root()
    }

    /**
     * Called when back button is pressed
     *
     * @return true if the press was intercepted
     */
    @Composable
    open fun onBack(): Boolean = false

    inline fun <reified T : AppSettingsBase> settings(): T = app.settings()
}