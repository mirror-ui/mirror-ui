package framework.app

import androidx.compose.runtime.Composable
import core.windowing.WindowManager
import lib.delegates.WriteOnce

abstract class RecentsActivity : Activity() {
    var windowManagerState by WriteOnce<WindowManager.State>()

    @Composable
    override fun Root() {
        RecentsView { }
    }

    @Composable
    abstract fun RecentsView(rootView: @Composable () -> Unit)
}