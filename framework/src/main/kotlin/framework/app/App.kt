package framework.app

import Paths
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toComposeImageBitmap
import androidx.compose.ui.renderComposeScene
import core.process.ProcessManager
import core.windowing.Window
import core.windowing.WindowManager
import framework.pkg.Package
import framework.settings.AppSettingsBase
import framework.settings.AppSettingsName
import lib.annotations.annotationOrNull
import lib.extensions.java.nio.file.div
import lib.reflect.asObject
import lib.serialization.createDefaultYamlIfNotExists
import lib.serialization.deserializeYamlOrEmpty
import lib.serialization.writeYaml
import lib.ui.theme.DefaultTheme
import logger
import org.jetbrains.skia.Bitmap
import java.io.File
import java.nio.file.Path
import java.util.*
import kotlin.reflect.KClass

class App(val pkg: Package) {
    internal val windowStack = LinkedList<Window>()
    var recentsPreview: ImageBitmap? = null
        private set
    // TODO: use translated app name
    val title: String = pkg.packageName

    val providesActivities get() = pkg.manifest.provides.apps.isNotEmpty()

    fun createActivity(className: String): Activity {
        logger.info("Creating activity ${pkg.packageName}/$className")
        return className.asObject<Activity>().apply {
            app = this@App
        }
    }

    fun launch() {
        logger.info("Launching app ${pkg.packageName}")
        ProcessManager.start(this)
        WindowManager.show(this)
    }

    @OptIn(ExperimentalComposeUiApi::class)
    fun pause() {
        windowStack.last?.let { window ->
            recentsPreview = renderComposeScene(window.size.width, window.size.height) {
                DefaultTheme {
                    window.activity()
                }
            }.toComposeImageBitmap()
        }
    }

    inline fun <reified T : AppSettingsBase> settings(): T =
        T::class.settingsFile().createDefaultYamlIfNotExists<T>().deserializeYamlOrEmpty<T>().apply {
            save = { saveSettings(this) }
        }

    fun KClass<out AppSettingsBase>.settingsFilePath(): Path =
        Paths.appDataDir / pkg.packageName / "${annotationOrNull(AppSettingsName::class)?.name ?: "settings"}.yaml"

    fun KClass<out AppSettingsBase>.settingsFile(): File = settingsFilePath().toFile()

    inline fun <reified T : AppSettingsBase> saveSettings(s: T): Unit = T::class.settingsFilePath().writeYaml(s)

}
