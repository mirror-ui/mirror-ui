package lib.concurrency

import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlin.reflect.KProperty

class Atomic<T>(private var value: T) {

    private val mutex = Mutex()

    operator fun setValue(thisObj: Any, property: KProperty<*>, value: T) {
        runBlocking {
            mutex.withLock {
                this@Atomic.value = value
            }
        }
    }

    operator fun getValue(thisObj: Any, property: KProperty<*>) =
        runBlocking {
            mutex.withLock {
                return@runBlocking value
            }
        }
}
