package lib.concurrency

import logger
import kotlin.concurrent.fixedRateTimer

class Timer(period: Long, var action: () -> Unit = {}) {
    private val timer: java.util.Timer

    init {
        logger.debug("Starting timer $this")
        timer = fixedRateTimer(
            daemon = true,
            period = period,
            action = {
                action()
            }
        )
    }

    fun stop() {
        logger.debug("Stopping timer $this")
        timer.cancel()
    }

}