package lib.extensions.io.ktor.http

import io.ktor.http.*

private val imATeapot = HttpStatusCode(418, "I'm a teapot")
val HttpStatusCode.Companion.ImATeapot get() = imATeapot
