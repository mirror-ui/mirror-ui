package lib.extensions.java.nio.file

import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.io.path.pathString

operator fun Path.plus(p: Path): Path = Path(pathString, p.pathString)
operator fun Path.div(p: Path): Path = plus(p)
operator fun Path.plus(p: String): Path = Path(pathString, p)
operator fun Path.div(p: String): Path = plus(p)
