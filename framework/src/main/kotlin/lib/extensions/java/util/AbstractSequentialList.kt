package lib.extensions.java.util

import java.util.*
import kotlin.reflect.full.createInstance

inline val <reified T> AbstractSequentialList<T>.cloned get() =
    this::class.createInstance().apply { addAll(this) }
