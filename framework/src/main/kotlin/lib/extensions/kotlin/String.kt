package lib.extensions.kotlin

import java.util.*

val String.base64: String get() = Base64.getEncoder().encodeToString(toByteArray())
