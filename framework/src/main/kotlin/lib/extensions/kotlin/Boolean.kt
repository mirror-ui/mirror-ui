package lib.extensions.kotlin

inline infix fun <R> Boolean.then(then: () -> R): R? {
    return if (this) {
        then()
    } else {
        null
    }
}
