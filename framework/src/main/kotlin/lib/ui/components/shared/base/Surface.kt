package lib.ui.components.shared.base

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import lib.ui.theme.colors

@Composable
fun StyledSurface(
    modifier: Modifier = Modifier,
    addBackgroundColor: Boolean = true,
    content: @Composable () -> Unit,
) {
    Surface(
        shape = RoundedCornerShape(17.dp),
        modifier = modifier.padding(8.dp),
        color = if (addBackgroundColor) colors().surface else colors().background,
        contentColor = colors().onSurface,
        content = content,
    )
}

@Composable
fun MainSurface(modifier: Modifier, content: @Composable () -> Unit) {
    Surface(
        modifier = modifier.fillMaxSize(),
        color = Color.Transparent,
        contentColor = colors().onBackground,
        content = content,
    )
}