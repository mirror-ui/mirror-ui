package lib.ui.components.shared

import androidx.compose.foundation.layout.LayoutScopeMarker
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Measurable
import androidx.compose.ui.layout.ParentDataModifier
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.unit.Density
import lib.ui.geometry.Point
import lib.ui.geometry.PointOffset

enum class AlignmentOffset {
    None, HalfOutside, Outside
}

@Stable
private data class AlignChildData(
    var horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    var verticalAlignment: Alignment.Vertical = Alignment.Top,
    var alignmentOffsetX: AlignmentOffset = AlignmentOffset.None,
    var alignmentOffsetY: AlignmentOffset = AlignmentOffset.None,
    var offset: PointOffset = Point.zero
)

@Stable
private data class HorizontalAlignChildData(
    val horizontalAlignment: Alignment.Horizontal
) : ParentDataModifier {
    override fun Density.modifyParentData(parentData: Any?): AlignChildData {
        if (parentData is AlignChildData) {
            parentData.horizontalAlignment = horizontalAlignment
        } else {
            return AlignChildData(horizontalAlignment = horizontalAlignment)
        }
        return parentData
    }
}

@Stable
private data class VerticalAlignChildData(
    val verticalAlignment: Alignment.Vertical
) : ParentDataModifier {
    override fun Density.modifyParentData(parentData: Any?): AlignChildData {
        if (parentData is AlignChildData) {
            parentData.verticalAlignment = verticalAlignment
        } else {
            return AlignChildData(verticalAlignment = verticalAlignment)
        }
        return parentData
    }
}

@Stable
private data class AlignmentOffsetChildData(
    val offsetX: AlignmentOffset?,
    val offsetY: AlignmentOffset?,
) : ParentDataModifier {
    override fun Density.modifyParentData(parentData: Any?): AlignChildData {
        if (parentData is AlignChildData) {
            offsetX?.let { parentData.alignmentOffsetX = it }
            offsetY?.let { parentData.alignmentOffsetY = it }
        } else {
            return AlignChildData().apply {
                offsetX?.let { alignmentOffsetX = offsetX }
                offsetY?.let { alignmentOffsetY = offsetY }
            }
        }
        return parentData
    }
}

@Stable
private data class OffsetChildData(
    val offset: PointOffset
) : ParentDataModifier {
    override fun Density.modifyParentData(parentData: Any?): AlignChildData {
        if (parentData is AlignChildData) {
            parentData.offset = offset
        } else {
            return AlignChildData(offset = offset)
        }
        return parentData
    }
}

@LayoutScopeMarker
class AlignmentLayoutScope {

    @Stable
    fun Modifier.alignTop() = then(
        VerticalAlignChildData(Alignment.Top)
    )

    @Stable
    fun Modifier.alignBottom() = then(
        VerticalAlignChildData(Alignment.Bottom)
    )

    @Stable
    fun Modifier.alignStart() = then(
        HorizontalAlignChildData(Alignment.Start)
    )

    @Stable
    fun Modifier.alignEnd() = then(
        HorizontalAlignChildData(Alignment.End)
    )

    @Stable
    fun Modifier.alignmentOffset(offsetX: AlignmentOffset? = null, offsetY: AlignmentOffset? = null) = then(
        AlignmentOffsetChildData(offsetX, offsetY)
    )

    @Stable
    fun Modifier.offset(offset: PointOffset) = then(
        OffsetChildData(offset)
    )

}

private val Measurable.alignChildData get() = parentData as? AlignChildData ?: AlignChildData()

private class PlaceableWrapper(val placeable: Placeable, val data: AlignChildData)

@Composable
fun AlignmentLayout(
    modifier: Modifier = Modifier,
    content: @Composable AlignmentLayoutScope.() -> Unit,
) {
    Layout(modifier = modifier, content = { AlignmentLayoutScope().content() }) { measurables, constraints ->
        val looseConstraints = constraints.copy(minWidth = 0, minHeight = 0)
        val items = measurables.map { PlaceableWrapper(it.measure(looseConstraints), it.alignChildData) }

        val height =
            (items.maxOfOrNull { it.placeable.height }?.coerceAtMost(looseConstraints.maxHeight) ?: 0)
                .coerceAtLeast(looseConstraints.minHeight)
        val width =
            (items.maxOfOrNull { it.placeable.width }?.coerceAtMost(looseConstraints.maxWidth) ?: 0)
                .coerceAtLeast(looseConstraints.minWidth)

        layout(width, height) {
            items.forEach { item ->
                val xPos = when (item.data.horizontalAlignment) {
                    Alignment.Start -> when (item.data.alignmentOffsetX) {
                        AlignmentOffset.Outside -> -item.placeable.width
                        AlignmentOffset.HalfOutside -> -item.placeable.width / 2
                        AlignmentOffset.None -> 0
                    }
                    Alignment.End -> when (item.data.alignmentOffsetX) {
                        AlignmentOffset.Outside -> width
                        AlignmentOffset.HalfOutside -> width - item.placeable.width / 2
                        AlignmentOffset.None -> width - item.placeable.width
                    }
                    else -> 0
                } + item.data.offset.x.toPx().toInt()
                val yPos = when (item.data.verticalAlignment) {
                    Alignment.Top -> when (item.data.alignmentOffsetY) {
                        AlignmentOffset.Outside -> -item.placeable.height
                        AlignmentOffset.HalfOutside -> -item.placeable.height / 2
                        AlignmentOffset.None -> 0
                    }
                    Alignment.Bottom -> when (item.data.alignmentOffsetY) {
                        AlignmentOffset.Outside -> height
                        AlignmentOffset.HalfOutside -> height - item.placeable.height / 2
                        AlignmentOffset.None -> height - item.placeable.height
                    }
                    else -> 0
                } + item.data.offset.y.toPx().toInt()
                item.placeable.placeRelative(xPos, yPos)
            }
        }
    }
}