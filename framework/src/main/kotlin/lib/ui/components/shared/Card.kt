package lib.ui.components.shared

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import lib.ui.components.shared.base.StyledSurface

@Composable
fun Card(modifier: Modifier = Modifier, addBackgroundColor: Boolean = true, content: @Composable () -> Unit) {
    StyledSurface(modifier = modifier, addBackgroundColor = addBackgroundColor, content = content)
}