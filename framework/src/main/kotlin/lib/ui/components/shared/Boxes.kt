package lib.ui.components.shared

import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.offset

@Composable
fun HorizontallyCenteredBox(
    modifier: Modifier = Modifier,
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentWidth(Alignment.CenterHorizontally),
        content = content
    )
}

@Composable
fun ClickableBox(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    shape: Shape = RectangleShape,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    backgroundColor: Color = Color.Transparent,
    content: @Composable () -> Unit,
) {
    Surface(
        modifier = modifier.let {
            if (enabled) {
                it.clickable(
                    onClick = onClick,
                    role = Role.Button,
                    interactionSource = interactionSource,
                    indication = null
                )
            } else it
        },
        shape = shape,
        color = backgroundColor,
        contentColor = backgroundColor,
        content = content,
    )
}

@Composable
fun FillingBox(
    modifier: Modifier = Modifier,
    content: @Composable BoxScope.() -> Unit
) {
    Box(
        modifier = modifier.fillMaxSize(),
        content = content,
    )
}

@Composable
fun FlexBoxRow(
    modifier: Modifier = Modifier,
    horizontalArrangement: Arrangement.Horizontal = Arrangement.Start,
    verticalAlignment: Alignment.Vertical = Alignment.Top,
    content: @Composable () -> Unit
) {
    Box(modifier) {
        Layout(content = content) { measurables, constraints ->
            val rows = mutableListOf<List<Placeable>>()
            var placeables = mutableListOf<Placeable>()
            var rowConstraints = constraints

            measurables.forEach { measurable ->
                val placeable = measurable.measure(Constraints())
                if (placeable.width !in rowConstraints.minWidth..rowConstraints.maxWidth) {
                    rows += placeables
                    rowConstraints = constraints
                    placeables = mutableListOf()
                }
                val consumedWidth = placeable.width
                rowConstraints = rowConstraints.offset(-consumedWidth)
                placeables.add(placeable)
            }
            rows += placeables

            val height = (rows.sumOf { row -> row.maxOfOrNull { it.height } ?: 0 }).coerceAtMost(constraints.maxHeight)
            val width = constraints.maxWidth.coerceAtLeast(rows.maxOf { row -> row.sumOf { it.width } })

            layout(width, height) {
                var yPos = verticalAlignment.align(rows.maxOf { row -> row.maxOfOrNull { it.height } ?: 0 }, height)
                rows.forEach { row ->
                    val rowWidth = row.sumOf { it.width }
                    var xPos = when (horizontalArrangement) {
                        Arrangement.End -> width - rowWidth
                        Arrangement.Center -> (width - rowWidth) / 2
                        Arrangement.SpaceAround -> if (row.isEmpty()) 0 else (width - rowWidth) / row.size / 2
                        else -> 0
                    }
                    row.forEachIndexed { ix, placeable ->
                        xPos += when (horizontalArrangement) {
                            Arrangement.SpaceBetween -> if (ix != 0) (width - rowWidth) / (row.size - 1) else 0
                            Arrangement.SpaceEvenly -> (width - rowWidth) / (row.size + 1)
                            Arrangement.SpaceAround -> if (ix != 0) (width - rowWidth) / row.size else 0
                            else -> 0
                        }
                        placeable.placeRelative(xPos, yPos)
                        xPos += placeable.width
                    }
                    yPos += row.maxOfOrNull { it.height } ?: 0
                }
            }
        }
    }
}

@Composable
fun FlexBoxColumn(
    modifier: Modifier = Modifier,
    horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    verticalArrangement: Arrangement.Vertical = Arrangement.Top,
    content: @Composable () -> Unit
) {
    Box(modifier) {
        Layout(content = content) { measurables, constraints ->
            val cols = mutableListOf<List<Placeable>>()
            var placeables = mutableListOf<Placeable>()
            var colConstraints = constraints

            measurables.forEach { measurable ->
                val placeable = measurable.measure(Constraints())
                if (placeable.height !in colConstraints.minHeight..colConstraints.maxHeight) {
                    cols += placeables
                    colConstraints = constraints
                    placeables = mutableListOf()
                }
                val consumedHeight = placeable.height
                colConstraints = colConstraints.offset(vertical = -consumedHeight)
                placeables.add(placeable)
            }
            cols += placeables

            val height = constraints.maxHeight.coerceAtLeast(cols.maxOf { col -> col.sumOf { it.height } })
            val width = (cols.maxOf { col -> col.maxOfOrNull { it.width } ?: 0 }).coerceAtMost(constraints.maxWidth)

            layout(height, width) {
                var xPos = horizontalAlignment.align(
                    cols.maxOf { col -> col.maxOfOrNull { it.width } ?: 0 }, width, layoutDirection
                )
                cols.forEach { col ->
                    val colHeight = col.sumOf { it.height }
                    var yPos = when (verticalArrangement) {
                        Arrangement.Bottom -> height - colHeight
                        Arrangement.Center -> (height - colHeight) / 2
                        Arrangement.SpaceAround -> if (col.isEmpty()) 0 else (height - colHeight) / col.size / 2
                        else -> 0
                    }
                    col.forEachIndexed { ix, placeable ->
                        yPos += when (verticalArrangement) {
                            Arrangement.SpaceBetween -> if (ix != 0) (height - colHeight) / (col.size - 1) else 0
                            Arrangement.SpaceEvenly -> (height - colHeight) / (col.size + 1)
                            Arrangement.SpaceAround -> if (ix != 0) (height - colHeight) / col.size else 0
                            else -> 0
                        }
                        placeable.placeRelative(xPos, yPos)
                        yPos += placeable.height
                    }
                    xPos += col.maxOfOrNull { it.width } ?: 0
                }
            }
        }
    }
}