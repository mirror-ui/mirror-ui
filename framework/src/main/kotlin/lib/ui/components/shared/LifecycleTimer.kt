package lib.ui.components.shared

import androidx.compose.runtime.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import lib.concurrency.Timer
import lib.concurrency.coroutines.launchIO
import logger

@Composable
fun LifecycleTimer(
    period: Long,
    immediate: Boolean = false,
    action: () -> Unit = {},
) {
    val timer = remember { Timer(period, action) }
    DisposableEffect(Unit) {
        onDispose {
            logger.debug("$this.onDispose() (timer $timer)")
            timer.stop()
        }
    }
    if (immediate) {
        LaunchedEffect(Unit) {
            logger.debug("$this: Running immediate action (timer $timer)")
            action()
        }
    }
}

@Composable
fun LifecycleCoroutineTimer(
    period: Long,
    immediate: Boolean = false,
    action: CoroutineScope.() -> Unit,
) {
    val coroutineScope = rememberCoroutineScope()
    LifecycleTimer(period, immediate) {
        coroutineScope.launch {
            action()
        }
    }
}

@Composable
fun LifecycleIOCoroutineTimer(
    period: Long,
    immediate: Boolean = false,
    ignoreErrors: Boolean = false,
    action: suspend CoroutineScope.() -> Unit,
) {
    val coroutineScope = rememberCoroutineScope()
    LifecycleTimer(period, immediate) {
        coroutineScope.launchIO {
            if (ignoreErrors) {
                try {
                    action()
                } catch (t: Throwable) {
                    logger.debug("Ignoring error", t)
                }
            } else action()
        }
    }
}