package lib.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

data class AdditionalColors(
    val actionBarWindowBackground: Color,
    val intermediaryBackground: Color,
    val success: Color,
    val transparentSecondaryOnBackground: Color,
    val transparentOnBackground: Color,
    val transparentBackground: Color,
    val lightTransparentBackground: Color,
)

val DarkColorPalette = darkColors(
    primary = Blue600,
    primaryVariant = Blue700,
    secondary = Gray900,
    secondaryVariant = Gray700,

    background = Black,
    surface = Gray900,
    onPrimary = Color.White,
    onSecondary = Color.White,
    onBackground = Color.White,
    onSurface = Gray20,
    error = Red400,
    onError = Gray980,
)

val LightColorPalette = lightColors(
    primary = Gray900,
    primaryVariant = Gray700,
    secondary = Blue600,
    secondaryVariant = Blue700,

    background = Gray200,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.White,
    onBackground = Color.Black,
    onSurface = Gray980,
    error = Red600,
    onError = Gray20,
)

@Composable
fun isInDarkTheme() = true

@Composable
fun colors() =
    if (isInDarkTheme()) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

val additionalDarkColors = AdditionalColors(
    actionBarWindowBackground = DarkColorPalette.background,
    intermediaryBackground = Gray950,
    success = Green400,
    transparentOnBackground = Color.White.copy(alpha = 0.8f),
    transparentSecondaryOnBackground = Color.White.copy(alpha = 0.5f),
    transparentBackground = DarkColorPalette.background.copy(alpha = 0.85f),
    lightTransparentBackground = DarkColorPalette.background.copy(alpha = 0.25f),
)

val additionalLightColors = AdditionalColors(
    actionBarWindowBackground = LightColorPalette.primary,
    intermediaryBackground = Gray20,
    success = Green600,
    transparentOnBackground = Color.Black.copy(alpha = 0.8f),
    transparentSecondaryOnBackground = Color.Black.copy(alpha = 0.5f),
    transparentBackground = LightColorPalette.background.copy(alpha = 0.85f),
    lightTransparentBackground = LightColorPalette.background.copy(alpha = 0.25f),
)

@Composable
fun additionalColors() =
    if (isInDarkTheme()) {
        additionalDarkColors
    } else {
        additionalLightColors
    }

@Composable
fun DefaultTheme(content: @Composable () -> Unit) {
    MaterialTheme(
        colors = colors(),
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}
