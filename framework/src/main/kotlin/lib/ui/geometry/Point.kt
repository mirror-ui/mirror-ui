package lib.ui.geometry

import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

data class Point(var x: Dp = 0.dp, var y: Dp = 0.dp) {

    operator fun unaryPlus() = Point(x, y)
    operator fun unaryMinus() = Point(-x, -y)
    operator fun plus(other: Point) = Point(x + other.x, y + other.y)
    operator fun minus(other: Point) = Point(x - other.x, y - other.y)

    operator fun plusAssign(other: Point) {
        x = x.plus(other.x)
        y = y.plus(other.y)
    }

    operator fun minusAssign(other: Point) {
        x = x.minus(other.x)
        y = y.minus(other.y)
    }

    fun add(both: Dp) = apply {
        x = x.plus(both)
        y = y.plus(both)
    }

    fun add(point: Point) = apply { this += point }
    fun subtract(point: Point) = apply { this -= point }

    fun clamp(
        minX: Dp,
        minY: Dp,
        maxX: Dp,
        maxY: Dp
    ) = apply {
        x = x.coerceIn(minX, maxX.coerceAtLeast(minX))
        y = y.coerceIn(minY, maxY.coerceAtLeast(minY))
    }

    fun copy() = Point(x, y)

    companion object {
        val zero get() = Point()
    }

}

typealias PointOffset = Point
