package lib.ui.geometry

import androidx.compose.ui.unit.Dp

data class Rectangle(var width: Dp, var height: Dp) {

    fun copy(): Rectangle {
        return Rectangle(width, height)
    }

    operator fun unaryPlus() = Rectangle(width, height)
    operator fun unaryMinus() = Rectangle(-width, -height)

    operator fun plus(other: Rectangle) = Rectangle(width + other.width, height + other.height)
    operator fun minus(other: Rectangle) = Rectangle(width - other.width, height - other.height)

    operator fun plusAssign(other: Rectangle) {
        width = width.plus(other.width)
        height = height.plus(other.height)
    }

    operator fun minusAssign(other: Rectangle) {
        width = width.minus(other.width)
        height = height.minus(other.height)
    }
}