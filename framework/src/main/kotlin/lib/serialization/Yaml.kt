package lib.serialization

import com.charleskorn.kaml.EmptyYamlDocumentException
import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.decodeFromStream
import com.charleskorn.kaml.encodeToStream
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.nio.file.Path
import kotlin.io.path.createDirectories

inline fun <reified R> String.deserializeYaml() = Yaml.default.decodeFromString(this) as R

inline fun <reified R> InputStream.deserializeYaml() = use { Yaml.default.decodeFromStream(this) as R }
inline fun <reified R> InputStream.deserializeYamlOrNull() = use {
    runCatching {
        Yaml.default.decodeFromStream(this) as R
    }.getOrNull()
}
inline fun <reified R> InputStream.deserializeYamlOrEmpty() = use {
    try {
        Yaml.default.decodeFromStream(this) as R
    } catch (_: EmptyYamlDocumentException) {
        "{}".deserializeYaml()
    }
}
val @Serializable Any.yaml get() = Yaml.default.encodeToString(this)
inline fun <reified R> OutputStream.writeYaml(value: R) = Yaml.default.encodeToStream(value, this)

inline fun <reified R> File.deserializeYaml() = inputStream().deserializeYaml<R>()
inline fun <reified R> File.deserializeYamlOrNull() = inputStream().deserializeYamlOrNull<R>()
inline fun <reified R> File.deserializeYamlOrEmpty() = inputStream().deserializeYamlOrEmpty<R>()
inline fun <reified R> File.writeYaml(value: R) = outputStream().use { it.writeYaml(value) }
inline fun <reified R> Path.writeYaml(value: R) = toFile().writeYaml(value)

inline fun <reified R> File.createDefaultYamlIfNotExists() = also {
    if (!exists()) {
        toPath().parent.createDirectories()
        writeYaml(R::class.java.getDeclaredConstructor().newInstance() as R)
    }
}

