package lib.random

object RandomStrings {
    fun alphanumeric(len: Int) =
        (1..len).map { (('a'..'z') + ('A'..'Z') + ('0'..'9')).random() }.joinToString("")

    fun alphabetic(len: Int) =
        (1..len).map { (('a'..'z') + ('A'..'Z')).random() }.joinToString("")
}