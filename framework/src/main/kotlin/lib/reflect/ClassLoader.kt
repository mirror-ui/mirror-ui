package lib.reflect

import systemClassLoader

inline fun <reified R> createObject(className: String) =
    (systemClassLoader.loadClass(className).getDeclaredConstructor().newInstance() as R)

inline fun <reified R> String.asObject(): R = createObject(this)
