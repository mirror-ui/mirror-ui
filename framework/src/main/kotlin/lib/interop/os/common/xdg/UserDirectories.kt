package lib.interop.os.common.xdg

import lib.interop.os.UserDirectories
import kotlin.io.path.Path
import kotlin.io.path.absolutePathString

object XDGUserDirectories : UserDirectories {
    override val config = System.getenv("XDG_CONFIG_HOME").orEmpty()
        .ifEmpty {
            System.getenv("HOME").orEmpty().ifEmpty { null }?.let {
                Path(it, ".config").absolutePathString()
            }
        }?.let { Path(it) }
    override val data = System.getenv("XDG_CONFIG_DATA").orEmpty()
        .ifEmpty {
            System.getenv("HOME").orEmpty().ifEmpty { null }?.let {
                Path(it, ".local", "share").absolutePathString()
            }
        }?.let { Path(it) }
}