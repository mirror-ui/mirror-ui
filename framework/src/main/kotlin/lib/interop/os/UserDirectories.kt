package lib.interop.os

import lib.interop.os.common.xdg.XDGUserDirectories
import lib.interop.os.windows.WindowsUserDirectories
import java.nio.file.Path
import kotlin.io.path.exists

interface UserDirectories {
    val config: Path?
    val data: Path?
}

val userDirectories by lazy {
    when {
        WindowsUserDirectories.config?.exists() ?: false -> WindowsUserDirectories
        XDGUserDirectories.config?.exists() ?: false -> XDGUserDirectories
        else -> throw IllegalStateException("Could not determine user directories. Your system is not compliant.")
    }
}