package lib.resources

import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.decodeFromStream
import java.io.InputStream

@JvmInline
value class ResourcePath(val path: String)

inline infix fun <R> ResourcePath.open(whileOpened: (InputStream) -> R): R =
    stream().use(whileOpened)

inline fun <reified T : Any> ResourcePath.loadYAML(): T =
    open {
        Yaml.default.decodeFromStream(it) as T
    }

fun ResourcePath.stream(): InputStream = javaClass.getResourceAsStream(path)
    ?: throw IllegalArgumentException("Resource path denoted by $this cannot be streamed")