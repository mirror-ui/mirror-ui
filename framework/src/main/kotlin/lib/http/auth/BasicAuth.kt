package lib.http.auth

import lib.extensions.kotlin.base64

object BasicAuth {
    fun basicAuthHeader(username: String, password: String) =
        "Basic ${"$username:$password".base64}"
}