package lib.delegates

import kotlin.reflect.KProperty

fun <T> holdValue() = ValueHolder<T>()

class ValueHolder<T> {
    var value: T? = null
    var hasValue: Boolean = false
        private set

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = value
        hasValue = true
    }

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return this.value!!
    }
}