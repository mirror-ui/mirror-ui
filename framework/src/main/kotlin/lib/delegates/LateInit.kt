package lib.delegates

import kotlin.reflect.KProperty

class LateInit<T> {
    private var holder = holdValue<T>()
    private var value by holder

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        if (!holder.hasValue) {
            throw IllegalStateException("Property must be initialized before use")
        }
        return value
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = value
    }

}
