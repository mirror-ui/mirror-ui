package lib.annotations

import kotlin.reflect.KClass

inline fun <reified A : Annotation> KClass<*>.annotationOrNull(annotation: KClass<A>) =
    annotations.find { it.annotationClass == annotation } as? A

inline fun <reified A : Annotation> KClass<*>.annotation(annotation: KClass<A>) : A =
    annotations.find { it.annotationClass == annotation } as? A
        ?: throw NotImplementedError("Missing annotation ${annotation.qualifiedName} on class $qualifiedName")