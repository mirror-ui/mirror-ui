package lib.annotations

@RequiresOptIn("Incomplete implementation", RequiresOptIn.Level.ERROR)
annotation class IncompleteImplementation

