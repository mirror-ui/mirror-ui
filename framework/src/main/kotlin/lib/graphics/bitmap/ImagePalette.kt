package lib.graphics.bitmap

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.luminance
import lib.extensions.androidx.compose.ui.graphics.darken
import lib.extensions.androidx.compose.ui.graphics.lighten
import lib.ui.theme.isInDarkTheme
import java.util.*

fun imagePaletteFrom(bitmap: ImageBitmap) = ImagePalette().applyPaletteFor(bitmap)

open class ColorPalette(
    var dominantColor: Color = Color.Unspecified,
    var lightDominantColor: Color = Color.Unspecified,
    var darkDominantColor: Color = Color.Unspecified,
) {
    @Composable
    fun foregroundDominant() = (if (isInDarkTheme()) lightDominantColor else darkDominantColor).let {
        if (it == Color.Unspecified) {
            if (isInDarkTheme()) {
                dominantColor.apply {
                    if (luminance() <= 0.25) {
                        lighten(0.25f)
                    }
                }
            } else {
                dominantColor.apply {
                    if (luminance() >= 0.75) {
                        darken(0.25f)
                    }
                }
            }
        } else it
    }

    @Composable
    fun backgroundDominant() = (if (isInDarkTheme()) darkDominantColor else lightDominantColor).let {
        if (it == Color.Unspecified) {
            if (isInDarkTheme()) {
                dominantColor.darken(0.5f)
            } else {
                dominantColor.lighten(0.5f)
            }
        } else it
    }
}

class RawColorPalette(
    dominantColorMap: Map<Int, Long>,
    lightDominantColorMap: Map<Int, Long>,
    darkDominantColorMap: Map<Int, Long>,
) {
    val dominantColor: Int
    val lightDominantColor: Int?
    val darkDominantColor: Int?
    val rank: Long
    val lightRank: Long
    val darkRank: Long

    init {
        val dominant = dominantColorMap.maxByOrNull { it.value }
        val lightDominant = lightDominantColorMap.maxByOrNull { it.value }
        val darkDominant = darkDominantColorMap.maxByOrNull { it.value }

        dominantColor = dominant!!.key
        lightDominantColor = lightDominant?.key
        darkDominantColor = darkDominant?.key

        rank = dominant.value
        lightRank = lightDominant?.value ?: 0
        darkRank = darkDominant?.value ?: 0
    }
}

class ImagePalette : ColorPalette() {

    fun applyPaletteFor(bitmap: ImageBitmap) = apply {
        // Map between color and number of occurrences
        val colorMap = LinkedList<RawColorPalette>()

        val pixBlock = IntArray(bitmap.width)
        for (y in 0 until bitmap.height) {
            val rowColorMap = sortedMapOf<Int, Long>()
            bitmap.readPixels(pixBlock, 0, y, bitmap.width, 1, 0, bitmap.width)
            pixBlock.forEach {
                if (rowColorMap.putIfAbsent(it, 0) != null) {
                    rowColorMap[it] = rowColorMap[it]!! + 1
                }
            }

            colorMap += RawColorPalette(
                dominantColorMap = rowColorMap,
                lightDominantColorMap = rowColorMap.filter { Color(it.key).luminance() >= 0.18 },
                darkDominantColorMap = rowColorMap.filter { Color(it.key).luminance() <= 0.65 },
            )
        }

        dominantColor = Color(colorMap.maxByOrNull { it.rank }!!.dominantColor)
        colorMap.maxByOrNull { it.lightRank }?.let { rcp ->
            rcp.lightDominantColor?.let {
                lightDominantColor = Color(it)
            }
        }
        colorMap.maxByOrNull { it.darkRank }?.let { rcp ->
            rcp.darkDominantColor?.let {
                darkDominantColor = Color(it)
            }
        }

    }

}