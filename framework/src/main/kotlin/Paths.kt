import lib.extensions.java.nio.file.div
import lib.interop.os.userDirectories
import kotlin.io.path.createDirectories

object Paths {
    private val configDir by lazy { (userDirectories.config!! / "mirrorui").apply { createDirectories() } }
    private val dataDir by lazy { (userDirectories.data!! / "mirrorui").apply { createDirectories() } }

    val settingsDir by lazy { configDir / "settings" }
    val appDataDir by lazy { dataDir / "apps" / "data" }

    // TODO: change to setting
    val wallpaperFile by lazy { settingsDir / "default_wallpaper.png" }
}