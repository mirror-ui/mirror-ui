lateinit var systemClassLoader: ClassLoader
    private set

fun setClassLoader(loader: ClassLoader) {
    if (::systemClassLoader.isInitialized) {
        throw IllegalStateException("System class loader has already been initialized")
    }

    systemClassLoader = loader
}

