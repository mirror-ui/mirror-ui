import io.ktor.client.*
import io.ktor.client.plugins.cache.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

val commonHttpClient = HttpClient {
    install(ContentNegotiation) {
        json(Json)
    }
    install(HttpCache)
}
