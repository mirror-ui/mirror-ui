package core.windowing

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import core.launcher.showLauncher
import core.systemui.SystemUIBase
import framework.app.Activity
import framework.app.App
import framework.pkg.PackageManager

object WindowManager {

    class State {
        var foregroundApp by mutableStateOf(null as App?)
        internal val foregroundWindow get() = foregroundApp?.windowStack?.last
        val recentsActivity by lazy { PackageManager.recents.first().apply { windowManagerState = this@State } }
        var showRecents by mutableStateOf(false)
        val windowDecoration get() =
            if (showRecents) recentsActivity.windowDecoration else foregroundWindow?.activity?.windowDecoration
        val windowCustomization get() =
            if (showRecents) recentsActivity.windowCustomization else foregroundWindow?.activity?.windowCustomization
    }

    private val state by lazy { State() }

    @Composable
    operator fun invoke() {
        if (state.showRecents) {
            state.recentsActivity.RecentsView {
                state.foregroundWindow?.let { it() }
            }
        } else {
            state.foregroundWindow?.let { it() }
        }
    }

    internal fun show(app: App, activity: Activity? = null) {
        state.foregroundApp = app
        state.showRecents = false
        if (app.windowStack.isEmpty()) {
            app.windowStack += Window(
                activity ?: app.createActivity(app.pkg.manifest.provides.initialActivity ?:
                throw IllegalStateException("No initial activity found for ${app.pkg.packageName}")))
        }
    }

    @Composable
    fun closeForegroundWindow() {
        state.foregroundWindow?.activity?.let {
            if (!it.onBack()) {
                it.app.windowStack.pop()
                if (it.app.windowStack.isEmpty()) {
                    showLauncher()
                }
            }
        }
    }

    fun SystemUIBase.prepare() = apply {
        windowManagerState = state
    }

}