package core.windowing

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import framework.app.Activity

internal class Window(
    val activity: Activity,
) {

    var size: IntSize = IntSize.Zero
        private set

    @Composable
    operator fun invoke() {
        Box(Modifier.fillMaxSize().onGloballyPositioned { size = it.size }) {
            activity()
        }
    }

}