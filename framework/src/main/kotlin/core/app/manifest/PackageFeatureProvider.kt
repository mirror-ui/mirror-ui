package core.app.manifest

import kotlinx.serialization.Serializable

@Serializable
data class PackageFeatureProviders(
    val apps: List<AppPackageFeatureConfiguration> = listOf(),
    val launchers: List<LauncherPackageFeatureConfiguration> = listOf(),
    val widgets: List<WidgetPackageFeatureConfiguration> = listOf(),
    val recents: List<RecentsPackageFeatureConfiguration> = listOf(),
    val systemUI: SystemUIPackageFeatureConfiguration? = null,
) {
  val initialActivity get() = apps.firstOrNull()?.mainActivity ?: launchers.firstOrNull()?.launcherActivity
}

interface PackageFeatureConfiguration

@Serializable
data class AppPackageFeatureConfiguration(
    val mainActivity: String,
) : PackageFeatureConfiguration

@Serializable
data class LauncherPackageFeatureConfiguration(
    val launcherActivity: String,
) : PackageFeatureConfiguration

@Serializable
data class RecentsPackageFeatureConfiguration(
    val recentsActivity: String,
) : PackageFeatureConfiguration

@Serializable
data class WidgetPackageFeatureConfiguration(
    val widgetView: String,
) : PackageFeatureConfiguration

@Serializable
data class SystemUIPackageFeatureConfiguration(
    val systemUIClass: String,
) : PackageFeatureConfiguration

