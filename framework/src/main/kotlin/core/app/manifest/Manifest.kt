package core.app.manifest

import kotlinx.serialization.Serializable
import lib.serialization.deserializeYaml
import java.io.InputStream

@Serializable
data class Manifest(
    val packageName: String,
    val provides: PackageFeatureProviders,
) {
    companion object {
        fun load(i: InputStream) = i.deserializeYaml<Manifest>()
    }
}