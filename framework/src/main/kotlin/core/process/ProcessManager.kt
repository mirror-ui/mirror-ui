package core.process

import framework.app.App
import java.util.*

object ProcessManager {
    private val runningAppsList = LinkedList<App>()

    val runningApps get() = runningAppsList.toTypedArray()

    fun start(app: App) {
        runningAppsList += app
    }

    fun stop(app: App) {
        runningAppsList -= app
    }
}