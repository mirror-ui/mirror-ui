package core.systemui

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import core.windowing.WindowManager
import lib.delegates.WriteOnce

abstract class SystemUIBase {
    var windowManagerState by WriteOnce<WindowManager.State>()

    @Composable
    abstract fun SystemUI(rootView: @Composable (modifier: Modifier) -> Unit)

    @Composable
    operator fun invoke(rootView: @Composable (modifier: Modifier) -> Unit) {
        SystemUI(rootView)
    }
}