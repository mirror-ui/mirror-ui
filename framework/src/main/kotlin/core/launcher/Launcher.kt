package core.launcher

import framework.settings.Settings

fun showLauncher() {
    Settings.systemSettings.defaultLauncher.app().run {
        while (windowStack.size > 1) {
            windowStack.pop()
        }
        launch()
    }

}