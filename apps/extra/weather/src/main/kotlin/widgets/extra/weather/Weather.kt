package widgets.extra.weather

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.FilterQuality
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.loadImageBitmap
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import apps.extra.weather.WeatherConfiguration
import apps.extra.weather.client.WeatherClient
import commonHttpClient
import framework.components.HomeWidget
import framework.widget.Widget
import io.ktor.client.call.*
import io.ktor.client.request.*
import lib.client.weather.models.WeatherCondition
import lib.client.weather.models.WeatherResponse
import lib.concurrency.coroutines.launchIO
import lib.ui.components.shared.LifecycleIOCoroutineTimer

class WeatherWidget : Widget() {
    private val config by lazy { settings<WeatherConfiguration>() }
    private val weatherClient by lazy { WeatherClient(config) }

    @Composable
    override fun View() {
        var weatherResponse by remember { mutableStateOf(null as WeatherResponse?) }
        LifecycleIOCoroutineTimer(300_000, immediate = true) {
            weatherResponse = weatherClient.weather()
        }

        HomeWidget {
            if (weatherResponse != null) {
                Weather(weatherResponse = weatherResponse!!)
            } else {
                NoWeatherInformation()
            }
        }
    }
}

@Composable
fun Weather(modifier: Modifier = Modifier, weatherResponse: WeatherResponse) {
    Row(modifier.fillMaxWidth().height(120.dp), horizontalArrangement = Arrangement.SpaceEvenly) {
        Column(
            modifier = Modifier.fillMaxWidth(0.25f).fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            WeatherUIImage(weatherResponse.current.condition, modifier = Modifier.fillMaxHeight(0.5f))
            Text(text = weatherResponse.current.condition.currentWeather, textAlign = TextAlign.Center)
        }
        Column(
            modifier = Modifier.fillMaxWidth().fillMaxHeight(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(text = weatherResponse.location.City, textAlign = TextAlign.Center, fontSize = 40.sp)
            Spacer(modifier = Modifier.padding(vertical = 5.dp))
            Text(
                text = weatherResponse.current.tempCelsius.toString() + " °C",
                textAlign = TextAlign.Center,
                fontSize = 30.sp
            )
        }
    }
}

@Composable
private fun WeatherUIImage(
    image: WeatherCondition,
    contentScale: ContentScale = ContentScale.Fit,
    modifier: Modifier = Modifier,
) {
    var imageBitmap by remember { mutableStateOf(null as ImageBitmap?) }
    LaunchedEffect(image.fullIconUrl) {
        launchIO {
            imageBitmap = loadImageBitmap(commonHttpClient.get(image.fullIconUrl).body())
        }
    }
    if (imageBitmap != null) {
        Image(
            modifier = modifier,
            bitmap = imageBitmap!!,
            contentDescription = "",
            contentScale = contentScale,
            filterQuality = FilterQuality.High,
        )
    }
}

@Composable
fun NoWeatherInformation(modifier: Modifier = Modifier) {
    Row(modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
        Text(text = "No Data. Please make sure you are connected to the internet!", textAlign = TextAlign.Center)
    }
}