package lib.client.weather.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WeatherData(
    @SerialName("temp_c")
    val tempCelsius: Float,
    @SerialName("temp_f")
    val tempFahrenheit: Float,
    val condition: WeatherCondition
)