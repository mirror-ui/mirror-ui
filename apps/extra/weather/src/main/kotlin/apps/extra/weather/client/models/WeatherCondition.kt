package lib.client.weather.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WeatherCondition(
    @SerialName("text")
    val currentWeather: String,
    val icon: String
) {
    val fullIconUrl get() = "https:$icon"
}
