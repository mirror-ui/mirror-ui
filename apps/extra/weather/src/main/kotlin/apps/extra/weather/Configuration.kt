package apps.extra.weather

import framework.settings.AppSettingsBase
import framework.settings.AppSettingsName
import kotlinx.serialization.Serializable

@AppSettingsName("config")
@Serializable
data class WeatherConfiguration(
    val city: String = "Tokyo",
    val key: String = "",
    val weatherApiUrl: String = "http://api.weatherapi.com/v1/current.json"
) : AppSettingsBase()