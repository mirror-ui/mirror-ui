package apps.extra.weather.client

import apps.extra.weather.WeatherConfiguration
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import lib.client.weather.models.WeatherResponse
import logger

class WeatherClient(private val config: WeatherConfiguration) {

    private var httpClient = HttpClient {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }

    suspend fun weather(city: String = config.city) = try {
        httpClient.get(config.weatherApiUrl) {
            parameter("q", city)
            parameter("key", config.key)
        }.body<WeatherResponse>()
    } catch (e: Exception) {
        logger.warn("Could not get weather information", e)
        null
    }
}