package apps.extra.spotify

import framework.settings.AppSettingsBase
import framework.settings.AppSettingsName
import kotlinx.serialization.Serializable
import java.net.URI
import java.net.URL

@AppSettingsName("config")
@Serializable
data class SpotifyConfiguration(
    val clientId: String = "",
    val clientSecret: String = "",
    val apiUrl: String = "https://api.spotify.com/v1",
    val authUrl: String = "https://accounts.spotify.com",
    val callbackUrl: String = "http://localhost:6200/integrations/spotify/callback",
    val enableIntegration: Boolean = true,
) : AppSettingsBase() {
    val canIntegrate get() = clientId.isNotBlank() && clientSecret.isNotBlank()

    val String.apiURI get() = URI(apiUrl + this)
    val String.authURI get() = URI(authUrl + this)
    val String.apiURL: URL get() = apiURI.toURL()
    val String.authURL: URL get() = authURI.toURL()
}