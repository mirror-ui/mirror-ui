package apps.extra.spotify.client.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PlaybackItem(
    val name: String,
    @SerialName("duration_ms")
    val durationMs: Int,
    val album: Album,
    val artists: List<Artist>,
) {
    val artistsString get() = artists.joinToString(", ") { artist -> artist.name }
}
