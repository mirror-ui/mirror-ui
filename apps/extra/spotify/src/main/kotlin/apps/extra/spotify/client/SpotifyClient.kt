package apps.extra.spotify.client

import apps.extra.spotify.SpotifyConfiguration
import apps.extra.spotify.SpotifyDataStore
import apps.extra.spotify.client.models.PlaybackState
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import logger

class SpotifyClient(config: SpotifyConfiguration, store: SpotifyDataStore) {
    val webClient = SpotifyWebClient(config, store)
    val player = Player(webClient, config)
}

class Player(private val webClient: SpotifyWebClient, private val config: SpotifyConfiguration) {
    suspend fun playbackState(): PlaybackState? = wrap {
        webClient.http.get("/me/player".apiURL) {
            webClient.headers(this)
        }.run {
            when (status) {
                HttpStatusCode.OK -> body<PlaybackState>()
                HttpStatusCode.NoContent -> null
                else -> null
            }
        }
    }

    suspend fun play() = wrap { webClient.http.put("/me/player/play".apiURL) }
    suspend fun pause() = wrap { webClient.http.put("/me/player/pause".apiURL) }
    suspend fun previous() = wrap { webClient.http.post("/me/player/previous".apiURL) }
    suspend fun next() = wrap { webClient.http.post("/me/player/next".apiURL) }

    private suspend fun <R> wrap(f: suspend SpotifyConfiguration.() -> R): R? {
        val stackTraceString = Throwable().stackTraceToString()
        try {
            return config.f()
        } catch (e: Exception) {
            logger.error("Request ran into exception", e)
            logger.debug("Caller stack trace:\n$stackTraceString")
            throw e
        }
    }
}

