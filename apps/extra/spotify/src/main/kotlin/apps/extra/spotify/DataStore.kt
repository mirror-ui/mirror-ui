package apps.extra.spotify

import framework.settings.AppSettingsBase
import framework.settings.AppSettingsName
import kotlinx.serialization.Serializable

@AppSettingsName("userdata")
@Serializable
data class SpotifyDataStore(
    var accessToken: String? = null,
    var refreshToken: String? = null,
    var tokenExpiresIn: Int = 0,
) : AppSettingsBase()
