package apps.extra.spotify.client

enum class GrantType(val value: String) {
    AuthorizationCode("authorization_code"), RefreshToken("refresh_token"),
}