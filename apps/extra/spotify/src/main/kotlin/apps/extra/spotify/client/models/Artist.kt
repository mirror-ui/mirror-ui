package apps.extra.spotify.client.models

import kotlinx.serialization.Serializable

@Serializable
data class Artist(val name: String)
