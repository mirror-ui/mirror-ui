package apps.extra.spotify.client

import apps.extra.spotify.SpotifyConfiguration
import apps.extra.spotify.SpotifyDataStore
import framework.settings.modify
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.Dispatchers.Unconfined
import kotlinx.coroutines.Job
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.serialization.json.Json
import lib.concurrency.coroutines.launchIO
import lib.http.auth.BasicAuth.basicAuthHeader
import lib.random.RandomStrings
import logger
import java.util.concurrent.Executors
import kotlin.random.Random

class SpotifyWebClient(
    private val config: SpotifyConfiguration,
    private val store: SpotifyDataStore,
) {
    private var httpClient = HttpClient {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
        install(HttpRequestRetry) {
            retryOnServerErrors(maxRetries = 3)
            retryOnException(maxRetries = 1)
            exponentialDelay(base = 2.0, maxDelayMs = 2000)
        }
        install(Auth) {
            bearer {
                loadTokens {
                    BearerTokens(accessToken!!, refreshToken!!)
                }
                refreshTokens {
                    if (refreshToken != null) {
                        logger.info("Access token expired, attempting to refresh it")
                        tryRefreshingSession()
                        BearerTokens(accessToken!!, refreshToken!!)
                    } else {
                        logger.info("Access token expired, but no refresh token available")
                        logout()
                        null
                    }
                }
            }
        }
        defaultRequest {
            headers(this)
        }
    }
    private var authHttpClient = HttpClient {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
        install(HttpRequestRetry) {
            retryOnServerErrors(maxRetries = 3)
            retryOnException(maxRetries = 1)
            exponentialDelay(base = 2.0, maxDelayMs = 2000)
        }
        defaultRequest {
            header("Authorization", basicAuthHeader(config.clientId, config.clientSecret))
        }
    }

    private val sessionMutex = Mutex()
    private val refreshMutex = Mutex()
    private val tokenMutex = Mutex()
    private lateinit var state: String
    private val dispatcher by lazy { Executors.newSingleThreadExecutor().asCoroutineDispatcher() }

    private var accessToken: String?
        get() = store.accessToken
        set(value) { store modify { accessToken = value } }
    private var refreshToken: String?
        get() = store.refreshToken
        set(value) { store modify { refreshToken = value } }
    private var expiresIn: Int
        get() = store.tokenExpiresIn
        set(value) { store modify { tokenExpiresIn = value }}

    private var refreshJob: Job? = null

    var onSessionChangeCallback: ((loggedIn: Boolean) -> Unit)? = null

    val http get() = httpClient

    val authorizeUrl
        get() = (config.run { "/authorize".authURL.toString() } + "?" + run {
            state = RandomStrings.alphanumeric(Random.nextInt(6, 8))
            ParametersBuilder().apply {
                append("response_type", "code")
                append("client_id", config.clientId)
                append(
                    "scope", listOf(
                        "user-read-playback-state",
                        "user-modify-playback-state",
                        "user-read-currently-playing",
                        "user-library-modify",
                        "user-library-read",
                        "streaming",
                        "app-remote-control",
                        "user-read-playback-position",
                        "user-top-read",
                        "user-read-recently-played",
                        "playlist-read-collaborative",
                        "playlist-read-private",
                    ).joinToString(" ")
                )
                append("redirect_uri", config.callbackUrl)
                append("state", state)
            }.build().formUrlEncode()
        }).apply { logger.debug("Created authorization URL $this") }

    suspend fun onReceiveCode(code: String, state: String) {
        logger.debug("Received auth code and state")
        if (state != this.state) {
            throw SecurityException("Received invalid state parameter. They do not match.")
        }
        if (code.isBlank()) {
            throw IllegalArgumentException("Received blank code. Needs to contain the auth code.")
        }

        handleBackendAuthentication(GrantType.AuthorizationCode, code)
    }

    private suspend fun handleBackendAuthentication(
        grantType: GrantType, authorizationCode: String? = null
    ) = tokenMutex.withLock {
        logger.info("Handling authentication with grant type $grantType")
        accessToken = null
        val response = acquireAccessToken(grantType, authorizationCode)
        logger.info("Successfully acquired access token")

        accessToken = response.accessToken
        response.refreshToken?.let { refreshToken = it }
        if (response.expiresIn != 0) {
            expiresIn = response.expiresIn
        }

        refreshMutex.withLock {
            refreshJob?.cancel()
            dispatcher.dispatch(Unconfined) {
                runBlocking {
                    logger.info("Token expires in ${expiresIn}s, auto-refresh active.")
                    // Wait for the token to expire and then refresh it
                    delay(expiresIn * 1000L - 10_000L)
                    launchIO {
                        tryRefreshingSession()
                    }
                }
            }
            logger.info("Auto-refresh has been set up.")
        }

        notifySessionChange()
    }

    fun headers(b: HttpMessageBuilder) {
        b.apply {
            header("Authorization", "Bearer $accessToken")
        }
    }

    val isLoggedIn get() = accessToken != null

    private fun logout() {
        logger.info("Logging out")
        accessToken = null
        refreshToken = null
        notifySessionChange()
    }

    private fun notifySessionChange() {
        logger.info("Session changed. Logged in: $isLoggedIn")
        onSessionChangeCallback?.invoke(isLoggedIn)
    }

    private suspend fun acquireAccessToken(
        grantType: GrantType, authorizationCode: String? = null,
    ): TokenResponse = sessionMutex.withLock {
        logger.info("Acquiring access token with grant type $grantType")
        val response = authHttpClient.submitForm(
            url = config.run { "/api/token".authURL.toString() }.also { logger.trace(it) },
            formParameters = Parameters.build {
                append("grant_type", grantType.value)
                when (grantType) {
                    GrantType.AuthorizationCode -> {
                        if (authorizationCode == null) {
                            throw IllegalArgumentException(
                                "authorizationCode must not be null when grantType == ${grantType.value}"
                            )
                        }
                        append("code", authorizationCode)
                        append("redirect_uri", config.callbackUrl)
                    }
                    GrantType.RefreshToken -> {
                        if (refreshToken != null) {
                            append("refresh_token", refreshToken!!)
                        } else {
                            throw IllegalArgumentException(
                                "refreshToken must not be null when grantType == ${grantType.value}"
                            )
                        }
                    }
                }
            }
        ) {}.body<TokenResponse>()

        logger.debug("Got a response from login service: $response")

        return response
    }

    /**
     * Attempts to refresh the current session using the refresh token
     * Otherwise just falls back to logging out and requiring fresh login.
     */
    private suspend fun tryRefreshingSession() {
        if (refreshToken == null) {
            logout()
            return
        }
        try {
            handleBackendAuthentication(GrantType.RefreshToken)
        } catch (e: Exception) {
            logger.warn("Failed to refresh session", e)
            logout()
        }
    }
}


