package apps.extra.spotify.client

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// https://developer.spotify.com/documentation/general/guides/authorization/code-flow/
@Serializable
data class TokenResponse(
    @SerialName("access_token")
    val accessToken: String,
    @SerialName("token_type")
    val tokenType: String? = null,
    val scope: String? = null,
    /**
     * Access Token validity time span in seconds
     */
    @SerialName("expires_in")
    val expiresIn: Int = 0,
    @SerialName("refresh_token")
    val refreshToken: String? = null,
) {
    val scopes get() = scope?.split(" ") ?: listOf()
}