package apps.extra.spotify.client.models

import kotlinx.serialization.Serializable

@Serializable
data class Album(
    val images: List<AlbumImage>,
    val name: String,
) {
    val firstImage get() = images.firstOrNull()
}
