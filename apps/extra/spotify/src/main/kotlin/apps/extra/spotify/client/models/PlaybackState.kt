package apps.extra.spotify.client.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PlaybackState(
    val item: PlaybackItem?,
    @SerialName("progress_ms")
    val progressMs: Int,
    @SerialName("is_playing")
    val isPlaying: Boolean
) {
    val progressFraction get() = item?.let { progressMs.toFloat() / item.durationMs } ?: 0.0f
    val albumImage get() = item?.album?.firstImage
}
