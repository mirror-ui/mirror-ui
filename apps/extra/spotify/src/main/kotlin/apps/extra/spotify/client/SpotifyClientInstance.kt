package apps.extra.spotify.client

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import lib.interop.onReceiveCode

class SpotifyClientComposeBridge(spotifyClient: SpotifyClient) {
    var isLoggedIn by mutableStateOf(spotifyClient.webClient.isLoggedIn)

    init {
        spotifyClient.webClient.onSessionChangeCallback = {
            isLoggedIn = it
        }
        onReceiveCode = { code, state -> spotifyClient.webClient.onReceiveCode(code, state) }
    }
}
