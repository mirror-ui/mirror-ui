package apps.extra.spotify.client.models

import kotlinx.serialization.Serializable

@Serializable
data class AlbumImage(
    val url: String,
    val height: Int,
    val width: Int,
)

