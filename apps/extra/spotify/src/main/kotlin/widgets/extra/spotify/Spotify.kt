package widgets.extra.spotify

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Pause
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.material.icons.filled.SkipNext
import androidx.compose.material.icons.filled.SkipPrevious
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.BlurredEdgeTreatment
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.FilterQuality
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.loadImageBitmap
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import apps.extra.spotify.SpotifyConfiguration
import apps.extra.spotify.SpotifyDataStore
import apps.extra.spotify.client.SpotifyClient
import apps.extra.spotify.client.SpotifyClientComposeBridge
import apps.extra.spotify.client.models.AlbumImage
import apps.extra.spotify.client.models.PlaybackState
import commonHttpClient
import framework.components.HomeWidget
import framework.widget.Widget
import io.github.g0dkar.qrcode.QRCode
import io.ktor.client.call.*
import io.ktor.client.request.*
import kotlinx.coroutines.launch
import lib.concurrency.coroutines.launchIO
import lib.extensions.androidx.compose.ui.graphics.darken
import lib.extensions.androidx.compose.ui.graphics.lighten
import lib.graphics.bitmap.ColorPalette
import lib.graphics.bitmap.ImagePalette
import lib.graphics.bitmap.imagePaletteFrom
import lib.ui.components.shared.IconFAB
import lib.ui.components.shared.LifecycleIOCoroutineTimer
import lib.ui.theme.additionalColors
import lib.ui.theme.colors
import lib.ui.theme.isInDarkTheme
import logger
import java.io.PipedInputStream
import java.io.PipedOutputStream

private enum class AlbumCoverMode {
    Full, BackgroundBlurred, Background, None
}

class SpotifyWidget : Widget() {
    private val config by lazy { settings<SpotifyConfiguration>() }
    private val dataStore by lazy { settings<SpotifyDataStore>() }
    private val spotifyClient by lazy { SpotifyClient(config, dataStore) }
    private val clientComposeBridge by lazy { SpotifyClientComposeBridge(spotifyClient) }

    private var savedPlaybackState by mutableStateOf(null as PlaybackState?)

    @Composable
    override fun View() {
        if (clientComposeBridge.isLoggedIn) {
            HomeWidget(addPadding = false) {
                SpotifyPlaybackWidget()
            }
        } else {
            HomeWidget {
                SpotifyLogin()
            }
        }
    }


    @Composable
    fun SpotifyPlaybackWidget() {
        LifecycleIOCoroutineTimer(1000, immediate = true, ignoreErrors = true) {
            savedPlaybackState = spotifyClient.player.playbackState()
        }

        if (savedPlaybackState != null) {
            MusicPlayer(savedPlaybackState!!)
        } else {
            Text(
                "Nothing is being played",
                modifier = Modifier.fillMaxWidth().padding(8.dp),
                textAlign = TextAlign.Center,
            )
        }
    }

    @Composable
    fun MusicPlayer(playbackState: PlaybackState) {
        val albumCoverMode by remember { mutableStateOf(AlbumCoverMode.BackgroundBlurred) }
        var imagePalette by remember { mutableStateOf(null as ImagePalette?) }

        Box(modifier = Modifier.height(IntrinsicSize.Min).run {
            imagePalette?.let {
                background(it.foregroundDominant().run {
                    if (isInDarkTheme()) darken(0.85f) else lighten(0.85f)
                })
            } ?: this
        }) {
            if (albumCoverMode in listOf(AlbumCoverMode.BackgroundBlurred, AlbumCoverMode.Background)) {
                BackgroundAlbumCover(
                    playbackState,
                    albumCoverMode == AlbumCoverMode.BackgroundBlurred,
                    onImagePalette = { imagePalette = it }
                )
            }
            Column(
                verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.padding(8.dp),
            ) {
                if (albumCoverMode == AlbumCoverMode.Full) {
                    FullAlbumCover(
                        playbackState,
                        modifier = Modifier.weight(1f),
                        onImagePalette = { imagePalette = it }
                    )
                }
                Column(horizontalAlignment = Alignment.CenterHorizontally) {
                    PlayingSongInfo(
                        playbackState,
                        if (albumCoverMode in listOf(AlbumCoverMode.BackgroundBlurred, AlbumCoverMode.Background)) null
                        else imagePalette
                    )
                    PlaybackControls(playbackState, imagePalette)
                }
            }
        }
    }

    @Composable
    private fun AlbumCoverImage(
        image: AlbumImage,
        contentScale: ContentScale = ContentScale.Fit,
        modifier: Modifier = Modifier,
        onImagePalette: (palette: ImagePalette) -> Unit = {},
    ) {
        var imageBitmap by remember { mutableStateOf(null as ImageBitmap?) }

        LaunchedEffect(image.url) {
            launchIO {
                logger.debug("Downloading new album cover")
                imageBitmap = loadImageBitmap(commonHttpClient.get(image.url).body())
                launch {
                    onImagePalette(imagePaletteFrom(imageBitmap!!))
                }
            }
        }
        if (imageBitmap != null) {
            Image(
                modifier = modifier,
                bitmap = imageBitmap!!,
                contentDescription = "",
                contentScale = contentScale,
                filterQuality = FilterQuality.High,
                alignment = Alignment.Center,
            )
        }
    }

    @Composable
    private fun BackgroundAlbumCover(
        playbackState: PlaybackState, blur: Boolean = false, onImagePalette: (palette: ImagePalette) -> Unit,
    ) {
        playbackState.albumImage?.let { image ->
            Surface(
                shape = RoundedCornerShape(9.dp),
                modifier = Modifier.run { if (blur) blur(15.dp, BlurredEdgeTreatment.Rectangle).scale(1.1f) else this }
            ) {
                AlbumCoverImage(
                    image,
                    ContentScale.Crop,
                    Modifier.fillMaxSize(),
                    onImagePalette,
                )
            }
        }
    }

    @Composable
    private fun FullAlbumCover(
        playbackState: PlaybackState, modifier: Modifier = Modifier, onImagePalette: (palette: ImagePalette) -> Unit
    ) {
        playbackState.albumImage?.let { image ->
            Box(modifier = modifier.padding(8.dp).defaultMinSize(minHeight = 192.dp).clip(RoundedCornerShape(9.dp))) {
                AlbumCoverImage(image = image, onImagePalette = onImagePalette)
            }
        }
    }

    @Composable
    fun PlayingSongInfo(playbackState: PlaybackState, palette: ColorPalette?) {
        Text(
            playbackState.item?.name ?: "",
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth().padding(8.dp),
            color = palette?.let {
                palette.foregroundDominant()
            } ?: additionalColors().transparentOnBackground,
        )
        Text(
            text = playbackState.item?.artistsString ?: "",
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth(),
            color = palette?.let {
                palette.foregroundDominant()
            } ?: additionalColors().transparentSecondaryOnBackground,
            fontSize = typography.subtitle2.fontSize,
        )
    }

    @Composable
    fun PlaybackControls(playbackState: PlaybackState, imagePalette: ImagePalette?) {
        val coroutineScope = rememberCoroutineScope()

        LinearProgressIndicator(
            modifier = Modifier.padding(10.dp).fillMaxWidth().clip(RoundedCornerShape(50)),
            progress = playbackState.progressFraction,
            backgroundColor = additionalColors().lightTransparentBackground,
            color = imagePalette?.foregroundDominant() ?: additionalColors().transparentOnBackground,
        )

        Row(
            horizontalArrangement = Arrangement.SpaceEvenly,
            modifier = Modifier.padding(8.dp).width(IntrinsicSize.Min)
        ) {
            IconFAB(
                onClick = { coroutineScope.launchIO { spotifyClient.player.previous() } },
                icon = Icons.Default.SkipPrevious,
                iconContentDescription = "SkipPrevious",
                backgroundColor = additionalColors().lightTransparentBackground,
            )
            IconFAB(
                onClick = {
                    coroutineScope.launchIO {
                        if (playbackState.isPlaying) {
                            spotifyClient.player.pause()
                        } else {
                            spotifyClient.player.play()
                        }
                        savedPlaybackState = spotifyClient.player.playbackState()
                    }
                },
                icon = if (playbackState.isPlaying) Icons.Default.Pause else Icons.Default.PlayArrow,
                iconContentDescription = "Play/Pause",
                backgroundColor = additionalColors().lightTransparentBackground,
            )
            IconFAB(
                onClick = {
                    coroutineScope.launchIO {
                        spotifyClient.player.next()
                        savedPlaybackState = spotifyClient.player.playbackState()
                    }
                },
                icon = Icons.Default.SkipNext,
                iconContentDescription = "SkipNext",
                backgroundColor = additionalColors().lightTransparentBackground,
            )
        }
    }

    @Composable
    fun SpotifyLoginQRCode() {
        val pipe = PipedInputStream()
        val pipeOut = PipedOutputStream().apply { connect(pipe) }
        var bmp by remember { mutableStateOf(null as ImageBitmap?) }

        if (bmp == null) {
            val fg = colors().onBackground.toArgb()
            val bg = colors().background.toArgb()
            LaunchedEffect(Unit) {
                launchIO {
                    pipeOut.use {
                        QRCode(spotifyClient.webClient.authorizeUrl)
                            .render(
                                brightColor = bg,
                                darkColor = fg,
                            ).writeImage(it)
                    }
                }
                launchIO {
                    bmp = loadImageBitmap(pipe)
                }
            }
        }
        bmp?.let {
            Image(
                bitmap = it,
                contentDescription = "",
                modifier = Modifier.padding(4.dp),
                contentScale = ContentScale.Fit,
                filterQuality = FilterQuality.High
            )
        }
    }

    @Composable
    fun SpotifyLogin(modifier: Modifier = Modifier) {
        Column(
            modifier = modifier.padding(12.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceAround
        ) {
            Text(text = "Log in", fontSize = typography.h6.fontSize, modifier = Modifier.padding(bottom = 4.dp))
            Text(
                text = "Scan the QR code below to log in",
                modifier = Modifier.padding(bottom = 4.dp),
                textAlign = TextAlign.Center,
            )
            Box(modifier = Modifier.background(colors().background)) {
                SpotifyLoginQRCode()
            }
        }
    }
}

