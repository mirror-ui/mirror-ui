package widgets.extra.clock

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import framework.components.HomeWidget
import framework.widget.Widget
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.time.LocalDateTime
import kotlin.concurrent.fixedRateTimer

object Clock {
    private var nonAtomicNow by mutableStateOf(LocalDateTime.now())
    private val mutex = Mutex()

    private suspend fun now(): LocalDateTime = mutex.withLock { nonAtomicNow }
    suspend fun setNow(value: LocalDateTime) = mutex.withLock { nonAtomicNow = value }

    fun nowBlocking() = runBlocking { now() }
}

private val clock by lazy {
    fixedRateTimer(
        daemon = true,
        period = 100,
        action = {
            runBlocking {
                launch(Dispatchers.Main) {
                    Clock.setNow(LocalDateTime.now())
                }
            }
        }
    )
    Clock
}

class DigitalClockWidget : Widget() {
    @Composable
    override fun View() {
        HomeWidget {
            Text(
                text = clock.nowBlocking().run { "%02d:%02d:%02d".format(hour, minute, second) },
                fontSize = 48.sp,
                fontWeight = FontWeight.Light,
            )
        }
    }
}
