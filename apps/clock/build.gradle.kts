import org.jetbrains.compose.compose
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
}

group = "dev.superboring.mirror"
version = "1.0.0"

repositories {
    mavenCentral()
}

val ktorVersion = "2.3.4"
dependencies {
    compileOnly(kotlin("stdlib"))
    compileOnly(compose.desktop.currentOs)
    compileOnly(project(":framework"))
    compileOnly("io.github.microutils:kotlin-logging:2.1.21")
}

task("appsClockJar", type = Jar::class) {
    archiveBaseName.set("clock")
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    with(tasks["jar"] as CopySpec)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    destinationDirectory.set(File("$rootDir/dist/apps"))
}
