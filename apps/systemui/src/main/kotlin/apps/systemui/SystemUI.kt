package apps.systemui

import Paths
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CropSquare
import androidx.compose.material.icons.outlined.ChevronLeft
import androidx.compose.material.icons.outlined.Circle
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.FilterQuality
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.loadImageBitmap
import androidx.compose.ui.unit.dp
import core.launcher.showLauncher
import core.systemui.SystemUIBase
import core.windowing.WindowManager
import framework.decoration.SystemUIProperties
import lib.concurrency.coroutines.launchIO
import lib.ui.components.shared.AlignmentLayout
import lib.ui.theme.colors

class SystemUI : SystemUIBase() {
    @Composable
    override fun SystemUI(rootView: @Composable (modifier: Modifier) -> Unit) {
        Box(modifier = Modifier.fillMaxSize()) {
            WindowBackground()
            SystemBarDrawModeSwitcher(
                fill = { modifier -> rootView(modifier) },
                bottomBar = { modifier -> Navbar(modifier) }
            )
        }
    }


    @Composable
    private fun WindowBackground() {
        Box(modifier = Modifier.fillMaxSize().run {
            windowManagerState.windowCustomization?.windowBackgroundBlur?.let {
                blur(it)
            } ?: this
        }) {
            if (Paths.wallpaperFile.toFile().exists()) {
                var wallpaperBitmap by remember { mutableStateOf(null as ImageBitmap?) }
                LaunchedEffect(Unit) {
                    launchIO {
                        Paths.wallpaperFile.toFile().inputStream().use {
                            wallpaperBitmap = loadImageBitmap(it)
                        }
                    }
                }
                wallpaperBitmap?.let {
                    Image(
                        modifier = Modifier.fillMaxSize(),
                        bitmap = it,
                        contentDescription = "",
                        contentScale = ContentScale.Crop,
                        filterQuality = FilterQuality.High,
                    )
                }
            }
        }
    }

    @Composable
    private fun SystemBarDrawModeSwitcher(
        fill: @Composable (modifier: Modifier) -> Unit,
        bottomBar: @Composable (modifier: Modifier) -> Unit,
    ) {
        if (windowManagerState.windowCustomization?.drawBehindSystemBars == true) {
            AlignmentLayout(modifier = Modifier.fillMaxSize()) {
                fill(Modifier.alignTop())
                bottomBar(Modifier.alignBottom())
            }
        } else {
            Column {
                Box(Modifier.weight(1f)) {
                    fill(Modifier)
                }
                bottomBar(Modifier)
            }
        }
    }

    @Composable
    private fun Navbar(modifier: Modifier) {
        Row(
            modifier = modifier
                .fillMaxWidth()
                .height(SystemUIProperties.navbarHeight)
                .background(windowManagerState.windowDecoration?.navbarBackground() ?: Color.Transparent),
            horizontalArrangement = Arrangement.Center,
        ) {
            NavbarBackButton()
            NavbarHomeButton()
            NavbarRecentsButton()
        }
    }

    @Composable
    private fun NavbarButton(onClick: @Composable () -> Unit, icon: ImageVector, contentDescription: String) {
        var clicked by remember { mutableStateOf(false) }
        if (clicked) {
            onClick()
            clicked = false
        }
        Button(
            modifier = Modifier.padding(horizontal = 8.dp),
            onClick = { clicked = true },
            shape = CircleShape,
            colors = NavbarButtonColors,
            elevation = ButtonDefaults.elevation(0.dp, 0.dp, 0.dp, 0.dp, 0.dp)
        ) {
            Icon(
                icon,
                contentDescription = contentDescription,
                tint = colors().onBackground,
            )
        }
    }

    @Composable
    private fun NavbarBackButton() {
        NavbarButton(
            onClick = { WindowManager.closeForegroundWindow() },
            icon = Icons.Outlined.ChevronLeft,
            contentDescription = "Back",
        )
    }

    @Composable
    private fun NavbarHomeButton() {
        NavbarButton(
            onClick = {
                windowManagerState.foregroundApp?.pause()
                showLauncher()
                      },
            icon = Icons.Outlined.Circle,
            contentDescription = "Home",
        )
    }

    @Composable
    private fun NavbarRecentsButton() {
        NavbarButton(
            onClick = {
                windowManagerState.foregroundApp?.pause()
                windowManagerState.showRecents = true
          },
            icon = Icons.Filled.CropSquare,
            contentDescription = "Recents",
        )
    }

    private object NavbarButtonColors : ButtonColors {
        @Composable
        override fun backgroundColor(enabled: Boolean): State<Color> {
            return rememberUpdatedState(colors().background.copy(alpha = .0f))
        }

        @Composable
        override fun contentColor(enabled: Boolean): State<Color> {
            return rememberUpdatedState(colors().onBackground)
        }
    }
}
