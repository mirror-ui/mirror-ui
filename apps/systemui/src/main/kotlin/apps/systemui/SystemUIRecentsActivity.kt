package apps.systemui

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.FilterQuality
import androidx.compose.ui.graphics.asComposeImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import core.process.ProcessManager
import framework.app.RecentsActivity
import lib.extensions.androidx.compose.ui.graphics.transparentize
import lib.ui.theme.colors

class SystemUIRecentsActivity : RecentsActivity() {
    @OptIn(ExperimentalAnimationApi::class)
    @Composable
    override fun RecentsView(rootView: @Composable () -> Unit) {
        windowDecoration
            .setWindowBackground(Color.Transparent)
            .setNavbarBackground(Color.Transparent)

        Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            ProcessManager.runningApps.filter { it.providesActivities }.forEach { app ->
                Column(
                    modifier = Modifier.clickable { app.launch() },
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    Text(app.title)
                    app.recentsPreview?.also {
                        Image(
                            modifier = Modifier.padding(48.dp),
                            bitmap = it,
                            contentDescription = "",
                            contentScale = ContentScale.Fit,
                            filterQuality = FilterQuality.Medium,
                        )
                    } ?: run {
                        Box(
                            modifier = Modifier
                                .fillMaxSize()
                                .scale(0.8f)
                                .background(colors().primary)
                        ) {}
                    }
                }
            }
        }
    }

}