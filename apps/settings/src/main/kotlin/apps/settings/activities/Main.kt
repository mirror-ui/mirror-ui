package apps.settings.activities

import androidx.compose.material.Button
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import framework.app.Activity

class Main : Activity() {

    @Composable
    override fun Root() {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text("Settings") },
                )
            }
        ) {
            Text("Welcome to the Settings app")
            Button(onClick = { }) { Text("Test test test") }
        }
    }

}