import org.jetbrains.compose.compose
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
}

group = "dev.superboring.mirror"
version = "1.0.0"

repositories {
    mavenCentral()
}

val composeVersion = rootProject.ext["composeVersion"]
dependencies {
    compileOnly(kotlin("stdlib"))
    compileOnly(compose.desktop.currentOs)
    compileOnly("org.jetbrains.compose.material:material-icons-extended:$composeVersion")
    compileOnly(project(":framework"))
}


task("appsSettingsJar", type = Jar::class) {
    archiveBaseName.set("settings")
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    with(tasks["jar"] as CopySpec)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    destinationDirectory.set(File("$rootDir/dist/apps"))
}
