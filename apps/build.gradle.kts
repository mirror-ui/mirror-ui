plugins {
    kotlin("jvm")
}

group = "dev.superboring.mirror"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {

}

task("allApps") {
    dependsOn(
        "home:appsHomeJar",
        "settings:appsSettingsJar",
        "extra:spotify:appsSpotifyJar",
        "clock:appsClockJar",
        "extra:weather:appsWeatherJar",
        "systemui:appsSystemUIJar",
    )
}