package apps.home.activities

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import framework.app.Activity
import framework.components.HomeWidget
import framework.decoration.SystemUIProperties.navbarBottomPadding
import framework.decoration.SystemUIProperties.navbarHeight
import framework.pkg.Package
import framework.pkg.PackageManager
import kotlinx.coroutines.launch
import lib.ui.components.shared.*
import lib.ui.geometry.PointOffset
import lib.ui.theme.colors
import kotlin.math.abs
import kotlin.math.absoluteValue
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.properties.Delegates

class Home : Activity() {

    @OptIn(ExperimentalMaterialApi::class)
    private lateinit var swipeMutState: MutableState<SwipeableState<Int>>

    private var maximizedAnchorPoint by Delegates.notNull<Float>()
    private var minimizedAnchorPoint by Delegates.notNull<Float>()

    private val widgets by lazy { PackageManager.widgets }

    @Composable
    fun QuickActionButtons() {
        HomeWidget {
            FlexBoxRow(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
                QuickActionButtonsGroup((1..8).map { Icons.Default.ThumbUp })
            }
        }
    }

    @Composable
    fun QuickActionButtonsGroup(
        iconButtons: List<ImageVector>
    ) {
        iconButtons.forEach {
            IconFAB(
                onClick = {},
                icon = it,
                iconContentDescription = "Test",
            )
        }
    }

    @Composable
    fun AppEntry(pkg: Package) {
        Column(
            modifier = Modifier.clickable { pkg.app.launch() },
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Box(modifier = Modifier.size(40.dp).clip(CircleShape).background(colors().primary))
            Text(pkg.packageName)
        }
    }

    @Composable
    fun AppList() {
        val packages = remember { PackageManager.packages.filter { it.manifest.provides.apps.isNotEmpty() } }

        FlexBoxRow(modifier = Modifier.padding(16.dp)) {
            packages.forEach { AppEntry(it) }
        }
    }

    @Composable
    fun Dock(dockHeight: Dp) {
        Box(modifier = Modifier.height(dockHeight))
    }

    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    fun AlignmentLayoutScope.AppDrawer(
        modifier: Modifier = Modifier,
        dockHeight: Dp,
        dockBackgroundColor: Color,
        onSwipeProgressUpdate: (swipeProgress: Float) -> Unit,
    ) = with(LocalDensity.current) {
        require(dockHeight > 0.dp)

        if (!::swipeMutState.isInitialized) {
            swipeMutState = mutableStateOf(SwipeableState(0))
        }
        val swipeState by swipeMutState
        var totalHeight by remember { mutableStateOf(null as Dp?) }
        minimizedAnchorPoint = (-dockHeight).toPx()
        maximizedAnchorPoint = -(totalHeight?.toPx() ?: 0f)
        val anchors = mapOf(maximizedAnchorPoint to 1, minimizedAnchorPoint to 0)
        val swipeProgress = 1f - (abs(swipeState.offset.value) / abs(maximizedAnchorPoint))
        val contentBlur = 9.dp * swipeProgress.pow(2)
        onSwipeProgressUpdate(swipeProgress)

        Box(
            modifier = modifier
                .onGloballyPositioned { coordinates ->
                    totalHeight = coordinates.size.toSize().height.toDp()
                }
                .offset(PointOffset(y = swipeState.offset.value.roundToInt().toDp()))
                .fillMaxSize()
                .swipeable(
                    state = swipeState,
                    anchors = anchors,
                    thresholds = { _, _ -> FractionalThreshold(0.25f) },
                    orientation = Orientation.Vertical
                ),
        ) {
            Surface(
                modifier = Modifier.fillMaxSize(),
                color = dockBackgroundColor,
                contentColor = colors().onBackground,
                shape = if (swipeProgress > 0f) RoundedCornerShape(
                    (swipeProgress.pow(2) * 21).dp
                ) else RectangleShape,
            ) {
                Column(
                    modifier =
                    Modifier
                        .fillMaxWidth()
                        .blur(contentBlur)
                        .navbarBottomPadding()
                ) {
                    Dock(dockHeight)
                    AppList()
                }
            }
        }
    }

    @Composable
    override fun Root() {
        val dockBackgroundColor = colors().background.copy(alpha = 0.45f)
        windowCustomization.drawBehindSystemBars = true
        windowDecoration
            .setWindowBackground(Color.Transparent)
            .setNavbarBackground(Color.Transparent)

        val dockHeight = 96.dp + navbarHeight
        var swipeProgress by remember { mutableStateOf(0f) }
        val reversedSwipeProgress = 1f - swipeProgress
        val reversedWithThresholdSwipeProgress = reversedSwipeProgress - 0.3f

        windowCustomization.windowBackgroundBlur = if (reversedWithThresholdSwipeProgress > 0.05f) {
            (reversedWithThresholdSwipeProgress.pow(2.5f) * 17).dp
        } else null

        AlignmentLayout(
            modifier = Modifier.fillMaxSize()
        ) {
            Column(
                modifier = Modifier.fillMaxSize().padding(bottom = dockHeight + 8.dp).run {
                    windowCustomization.windowBackgroundBlur?.let {
                        blur(it)
                    } ?: this
                },
                verticalArrangement = Arrangement.SpaceBetween,
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    modifier = Modifier.fillMaxWidth().weight(1f),
                ) {
                    Column(modifier = Modifier.weight(1f)) {
                        QuickActionButtons()
                    }
                    Column(modifier = Modifier.fillMaxWidth(0.5f)) {
                        widgets.forEach {
                            it.View()
                        }
                    }
                }
            }
            AppDrawer(
                modifier = Modifier.alignBottom().alignmentOffset(offsetY = AlignmentOffset.Outside),
                dockHeight = dockHeight,
                dockBackgroundColor = dockBackgroundColor,
                onSwipeProgressUpdate = { swipeProgress = it }
            )
        }
    }

    @OptIn(ExperimentalMaterialApi::class)
    @Composable
    override fun onBack(): Boolean = rememberCoroutineScope().run {
        val swipeState by swipeMutState
        if (swipeState.progress.fraction > 0f) {
            launch {
                // for whatever reason this does not work:
                //swipeState.animateTo(1, TweenSpec(200))
                // so we just do it without animation
                if (swipeState.progress.fraction > 0f) {
                    swipeState.performDrag((maximizedAnchorPoint - minimizedAnchorPoint).absoluteValue)
                }
            }
        }
        return true
    }
}