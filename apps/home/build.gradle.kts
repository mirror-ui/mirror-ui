import org.jetbrains.compose.compose
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    id("org.jetbrains.compose")
}

group = "dev.superboring.mirror"
version = "1.0.0"

repositories {
    mavenCentral()
}

val ktorVersion = "2.3.4"
val composeVersion = rootProject.ext["composeVersion"]
dependencies {
    compileOnly(kotlin("stdlib"))
    compileOnly(compose.desktop.currentOs)
    compileOnly("org.jetbrains.compose.material:material-icons-extended:$composeVersion")
    compileOnly(project(":framework"))
    implementation("io.github.g0dkar:qrcode-kotlin-jvm:3.3.0")
    compileOnly("io.ktor:ktor-client-core:$ktorVersion")
    compileOnly("io.ktor:ktor-client-java:$ktorVersion")
    compileOnly("io.ktor:ktor-client-auth:$ktorVersion")
    compileOnly("io.ktor:ktor-serialization-kotlinx-json:$ktorVersion")
    compileOnly("io.ktor:ktor-client-content-negotiation:$ktorVersion")
    compileOnly("io.github.microutils:kotlin-logging:3.0.5")
}


task("appsHomeJar", type = Jar::class) {
    archiveBaseName.set("home")
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    with(tasks["jar"] as CopySpec)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
    destinationDirectory.set(File("$rootDir/dist/apps"))
}
