import framework.pkg.PackageManager
import java.net.URL
import java.net.URLClassLoader

class DynamicClassLoader(private val loader: ClassLoader) : ClassLoader(loader) {
    private lateinit var urlClassLoader: URLClassLoader
    private val loadUrls = arrayListOf<URL>()

    override fun loadClass(name: String?): Class<*> {
        return try {
            loader.loadClass(name)
        } catch (e: ClassNotFoundException) {
            urlClassLoader.loadClass(name)
        }
    }

    private fun appendToClassPathForInstrumentation(file: String) {
        addToClassPath(file.asFileURL())
    }

    private fun String.asFileURL() = URL("file:$this")

    private fun addToClassPath(vararg urls: URL) {
        loadUrls.addAll(urls)
        val oldLoader = if (::urlClassLoader.isInitialized) urlClassLoader else null
        urlClassLoader = URLClassLoader(loadUrls.toTypedArray(), this)
        oldLoader?.close()
    }

    fun discoverApps() {
        addToClassPath(*PackageManager.packages.map { it.fileName.asFileURL() }.toTypedArray())
    }
}