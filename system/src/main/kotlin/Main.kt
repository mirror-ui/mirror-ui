import androidx.compose.ui.window.Window
import androidx.compose.ui.window.WindowPlacement
import androidx.compose.ui.window.WindowState
import androidx.compose.ui.window.application
import core.launcher.showLauncher
import core.windowing.WindowManager
import framework.pkg.PackageManager
import lib.ui.components.shared.base.MainSurface
import lib.ui.theme.DefaultTheme
import server.AppServer
import kotlin.system.exitProcess

private val fullScreen by lazy {
    System.getenv("FULLSCREEN")?.toBoolean() ?: false
}

private val systemUI by lazy { PackageManager.systemUI }

fun main() {
    val loader = Thread.currentThread().contextClassLoader as? DynamicClassLoader
    loader?.let {
        it.discoverApps()
        setClassLoader(it)
    }

    showLauncher()
    app()
}

fun app() = application {
    val autoLoadComponents = listOf<Any>(
        AppServer
    )
    logger.debug("Debug log is active")
    logger.info("${autoLoadComponents.size} autoload components are active")


    logger.info("Opening Window")
    Window(
        onCloseRequest = {
            exitApplication()
            exitProcess(0)
        },
        title = "Mirror UI",
        undecorated = fullScreen,
        resizable = !fullScreen,
        alwaysOnTop = fullScreen,
        state = WindowState(
            placement =
            if (fullScreen) WindowPlacement.Fullscreen
            else WindowPlacement.Floating
        )
    ) {
        DefaultTheme {
            systemUI { modifier ->
                MainSurface(modifier) {
                    WindowManager()
                }
            }
        }
    }
}
