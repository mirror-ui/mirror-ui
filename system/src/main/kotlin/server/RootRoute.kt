package server

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import lib.extensions.io.ktor.http.ImATeapot
import server.routes.integrationsRoute

fun Route.rootRoute() {
    route("/") {
        get {
            call.respondText("I'm a teapot", status = HttpStatusCode.ImATeapot)
        }
        integrationsRoute()
    }
}
