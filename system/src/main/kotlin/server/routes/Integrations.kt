package server.routes

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import server.routes.integrations.spotifyRoute

fun Route.integrationsRoute() {
    route("integrations") {
        get {
            call.respondText("Integrations endpoint")
        }
        spotifyRoute()
    }
}