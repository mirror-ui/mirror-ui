package server.routes.integrations

import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import lib.interop.onReceiveCode
import logger


fun Route.spotifyRoute() {
    route("spotify") {
        get {
            call.respondText("Spotify integration")
        }
        get("callback") {
            runCatching {
                logger.debug("Received login callback with query string ${call.request.queryString()}")
                val code = call.request.queryParameters["code"] ?: throw IllegalArgumentException("missing code param")
                val state = call.request.queryParameters["state"] ?: throw IllegalArgumentException("missing state param")

                onReceiveCode(code, state)

                call.respondText("Login successful. You can now close this tab.")
            }.exceptionOrNull()?.let {
                call.respondText("Login failed. Please check logs.")
            }
        }
    }
}