package server

import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.routing.*
import logger

object AppServer {

    init {
        start()
    }

    private fun start() {
        logger.info("Starting embedded app server")
        embeddedServer(Netty, port = 6200) {
            install(ContentNegotiation) {
                json()
            }

            routing {
                rootRoute()
            }
        }.start()
    }

}