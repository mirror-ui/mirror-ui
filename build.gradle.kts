import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    kotlin("plugin.serialization")
    id("org.jetbrains.compose")
}

group = "dev.superboring.mirror"
version = "1.0.0"

project.ext.apply {
    set("ktorVersion", "2.3.4")
    set("composeVersion", extra["compose.version"] as String)
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.github.microutils:kotlin-logging:3.0.5")
    implementation(project(":system"))
    implementation(project(":framework"))
}

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(17)
    }
}

tasks.withType<KotlinCompile> {
    dependsOn(":apps:allApps")
}

tasks.getByName<Delete>("clean") {
    delete.add("dist")
}

task("cake") {
    dependsOn(
        ":system:systemJar",
        ":framework:frameworkJar",
        ":apps:allApps",
    )
}