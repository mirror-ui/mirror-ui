pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }

    plugins {
        kotlin("jvm").version(extra["kotlin.version"] as String)
        kotlin("plugin.serialization").version(extra["kotlin.version"] as String)
        id("org.jetbrains.compose").version(extra["compose.version"] as String)
    }
}

rootProject.name = "mirror-ui"
include("framework")
include("apps")
include("apps:home")
include("apps:settings")
include("apps:extra:spotify")
include("apps:clock")
include("apps:extra:weather")
include("apps:systemui")
include("system")
